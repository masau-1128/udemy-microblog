from fabric.api import cd, sudo, shell_env, run, env

HOST_PATH = "/opt/django/venv/microblog/microblog"

ex_env = {
    "DJANGO_SETTINGS_MODULE": "microblog.settings.prod"
}

# env.hosts = ["10.0.2.15:2222", ]
# env.user = "root"
# env.password = "vagrant"


def collect_static():
    with cd("/opt/django/venv/microblog/microblog"):
        with shell_env(**ex_env):
            run("/opt/django/venv/bin/python manage.py collectstatic --no-input")

def git_update():
    with cd(HOST_PATH):
        run("sudo git pull")

def restart():
    with cd(HOST_PATH):
        with shell_env(**ex_env):
            #sudo("pkill -f gunicorn")

            sudo("setenforce 0")
            sudo("/opt/django/venv/bin/gunicorn -c gunicorn.conf.py microblog.wsgi")

            #run("/opt/django/venv/bin/python manage.py runserver &")

def all():
    git_update()
    collect_static()
    restart()