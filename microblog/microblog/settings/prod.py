from .common import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['*', ]

INSTALLED_APPS += (
	'gunicorn',
)

DATABASES = {
    'default': {
	'ENGINE': 'django.db.backends.postgresql_psycopg2',
	'NAME': 'microblog',
	'USER': 'microblog',
	'PASSWORD': 'microblog',
    }
}
