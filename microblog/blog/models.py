from django.db import models


# Create your models here.

class Blog(models.Model):
    # id = 1, 2, , 4,
    content = models.CharField(max_length=140)
    posted_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-posted_date', ]
